def quarter_of (month):
    """ Определяет, к какой четверти года относится месяц (месяц -
    число от 1 до 12) """
    if month in range(1, 4):
        return 1
    elif month in range(4, 7):
        return 2
    elif month in range(7, 10):
        return 3
    elif month in range(10, 13):
        return 4
