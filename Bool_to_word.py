def bool_to_word (boolean):
    """ Возвращает 'Yes' для 'True', 'No' для 'False'"""
    return "Yes" if boolean else "No"
