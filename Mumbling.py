def accum(s):
    """ Имитация заикания - повторить каждую букву на один раз
    больше, чем предыдущую: accum("abcd") -> "A-Bb-Ccc-Dddd" """
    count = 0
    new_word = []
    for letter in s:
        count += 1
        new_word += (letter * count).capitalize() + '-'
    return ''.join(new_word)[:-1]
