def high_and_low(numbers):
    """Возвращает максимальное и минимальное значение из заданной
    строки"""
    numbers = [int(x) for x in numbers.split(" ")]
    return str(max(numbers)) + " " + str(min(numbers))