def solution (string):
    """ Возвращает символы строки в обратном порядке"""
    return string [::-1]