def find_uniq (arr):
    """ Возвращает первое уникальное число из заданного массива"""
    arr_new = set(arr)
    return [num for num in arr_new if arr.count(num) == 1][0]
