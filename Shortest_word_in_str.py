def find_short(s):
    """ Возвращает длину самого короткого слова в строке """
    return min(len(i) for i in s.split())
