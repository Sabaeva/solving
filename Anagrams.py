def anagrams(word, words):
    """ Принимает слово и список слов, возвращается все анаграммы слова из
    списка
    anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']) --> ['aabb', 'bbaa'])"""
    return [elem for elem in words if sorted(elem) == sorted(word)]


print(anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']))
