def sum_two_smallest_numbers(numbers):
    """ Принимает последовательность чисел, возвращает сумму двух наименьших
    sum_two_smallest_numbers([5, 8, 12, 18, 22]) ----> 13"""
    return sum(sorted(numbers[:2]))


print(sum_two_smallest_numbers([5, 8, 12, 18, 22]))