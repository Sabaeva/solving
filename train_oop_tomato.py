class Tomato:
    states = {1: 'Отсутствует', 2: 'Цветение', 3: 'Зеленый', 4: 'Красный'}

    def __init__(self, index):
        self._index = index
        self._state = 1

    def grow(self):
        """ Переводит томат на следующую стадию созревания"""
        self._change_state()

    def is_ripe(self):
        """ Проверяет, созрел ли томат """
        if self._state == 4:
            return True
        return False

    # Защищенные методы
    def _change_state(self):
        if self._state < 4:
            self._state += 1
        self._print_state()

    def _print_state(self):
        print(f'Томат номер {self._index} - {Tomato.states[self._state]}')


class TomatoBush:

    def __init__(self, num_tomato):
        self.tomatoes = [Tomato(i) for i in range(1, num_tomato+1)]

    def grow_all(self):
        """ Переводит все томаты на следующую стадию созревания"""
        for tomato in self.tomatoes:
            tomato.grow()

    def are_all_ripe(self):
        """ Проверяет, все ли томаты созрели"""
        return all([tomato.is_ripe() for tomato in self.tomatoes])

    def give_away_all(self):
        """ Очищает список томатов после сбора урожая """
        self.tomatoes.clear()


class Gardener:

    def __init__(self, name, plant):
        """ Инициализируем садовника и растение"""
        self.name = name
        self._plant = plant

    def work(self):
        """ Когда садовник работает, растение зреет """
        print('Садовник работает')
        self._plant.grow_all()

    def harvest(self):
        """ Проверяет, все ли томаты созрели, если да, происходит сбор урожая"""
        if self._plant.are_all_ripe():
            self._plant.give_away_all()
            print('Урожай успешно собран!')
        else:
            print('Пока рано собирать урожай! ')


if __name__ == '__main__':
    tomato_bush = TomatoBush(3)
    gardener = Gardener('Emilio', tomato_bush)
    gardener.work()
    gardener.work()
    gardener.harvest()
    gardener.work()
    gardener.harvest()
