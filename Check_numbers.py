import re

""" Проверка телефонных номеров - действующий должен состоять из 8 чисел и 
начинаться с 1,8 или 9"""
pattern = r"^(1|8|9)\d{7}$"
while True:
    number = input(' Введите номер: ')
    if re.match(pattern, number):
        print('Valid')
    else:
        print('Invalid')
