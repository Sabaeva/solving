def sort_array (arr):
    """ Сортирует нечетные числа по возрастанию,четные числа
    остаются на своих местах. """
    odd = []
    ind_odd = []
    if arr == []:
        return arr
    else:
        for item in enumerate(arr):
            if item[1] % 2 > 0:
                odd.append(item[1])
                ind_odd.append(item[0])
            else:
                continue
    odd.sort()
    for i in range(0, ind_odd.__len__()):
        if ind_odd[i] != 0:
            arr[ind_odd[i]] = odd[i]
        else:
            arr[ind_odd[i]] = odd[i]
    return arr
