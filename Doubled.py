def maps(a):
    """ Принимает список чисел, возвращает их удвоенное значение
    maps([1, 2, 3]) --> [2, 4, 6])"""
    return list(map(lambda a: a * 2, a))


print(maps([1, 2, 3]))
