def filter_list(l):
    """ Принимает лист из строк и неотрицательных чисел, возвращает
    только числа """
    return [i for i in l if type(i) != str]
