def pig_it (text):
    """ Latin Pig - переставляет первую букву слова в конец и
    прибавляет 'ay'  """
    t = []
    for item in text.split():
        if item.isalpha():
            t.append(item[1:item.__len__()] + item[0] + 'ay')
        else:
            t.append(item)
    return ' '.join(t)
