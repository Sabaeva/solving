def get_middle(s):
    """ Возвращает среднюю букву/буквы слова
    get_middle("middle") --> "dd" """
    if len(s) % 2 == 0:
        return s[int(len(s) / 2)-1 : int(len(s) / 2)+1]
    else:
        return s[int(len(s) / 2)]


print(get_middle("testing"))
