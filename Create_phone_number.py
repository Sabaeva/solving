def create_phone_number(n):
    """ Получает список из 10 чисел от 0 до 9, возвращает
    телефонный номер.
    create_phone_number([1, 2, 3, 4, 5, 6, 7, 8, 9, 0])  =>
    returns "(123) 456-7890" """
    return '({0}{1}{2}) {3}{4}{5}-{6}{7}{8}{9}'.format(*n)


print(create_phone_number([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]))
