def open_or_senior(data):
    """ Принимает список из кортежей в формате [(возраст, способности(от-2
    до 26)), (), ...]
    Возвращает Open или Senior (если возраст >= 55, способности > 7 """
    members = []
    for member in data:
        if member[0] >= 55 and member[1] > 7:
            members.append('Senior')
        else:
            members.append('Open')
    return members


#print(open_or_senior([(16, 23),(73,1),(56, 20),(1, -1)]))
