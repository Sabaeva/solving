def is_isogram (string):
    """Проверяет является ли слово изограмой - содержит только
    уникальные буквы"""
    for char in string.lower():
        if string.lower().count(char) > 1:
            return False
        else:
            continue
    return True
