def count_positives_sum_negatives (arr):
    """ Возвращает количество положительных элементов и сумму
    отрицательных"""
    count_pos = 0
    sum_neg = 0
    if arr == []:
        return arr
    else:
        for num in arr:
            if num > 0:
                count_pos += 1
            else:
                sum_neg += num
    return [count_pos, sum_neg]
