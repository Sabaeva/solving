def month_to_season(month):
    """ Функция принимает номер месяца (int) и возвращает название сезона,
    к которому он относится """
    seasons = {
        (1, 2, 12): 'Зима',
        (3, 4, 5): 'Весна',
        (6, 7, 8): 'Лето',
        (9, 10, 11): 'Осень',
    }

    for key in seasons.keys():
        if month in key:
            return seasons[key]
    else:
        return 'Неверный номер месяца'


if __name__ == '__main__':
    print(month_to_season(11))
