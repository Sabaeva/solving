def tower_builder(n_floors):
    """Строит башню из '*' заданной высоты"""
    tower = []
    for i in range(1,n_floors+1):
        tower.insert(i,' '*abs(n_floors-i)+'*'*(i*2-1)+' '*abs(n_floors-i))
    return tower
