def namelist (names):
    if names == []:
        return ''
    elif len(names) == 1:
        return names[0]['name']
    elif len(names) == 2:
        return names[0]['name'] + ' & ' + names[1]['name']
    else:
        i = 0
        new_names = ''
        while i < len(names) - 2:
            new_names += names[i]['name'] + ', '
            i += 1
        return new_names + names[-2]['name'] + ' & ' + names[-1]['name']
