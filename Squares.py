def is_square (n):
    """ Является ли заданное целое число квадратом другого числа """
    if n >= 0:
        return (n ** 0.5).is_integer()
    else:
        return False