def find_it (seq):
    """ Возвращает то число, которое повторяется нечетное количество
    раз """
    return [num for num in seq if seq.count(num) % 2 == 1][0]
