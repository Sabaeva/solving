def to_camel_case(text):
    """Переводит строку в camelCase 
    to_camel_case("the-stealth-warrior") >  'theStealthWarrior'
    to_camel_case("The_Stealth_Warrior") >  'TheStealthWarrior' """
    new=[]
    for item in text.replace('_','-').split('-'):
        new.append(item.capitalize())
    return text.replace('_','-').split('-')[0] + ''.join(new[1:len(new)])