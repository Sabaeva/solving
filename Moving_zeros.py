def move_zeros (array):
    """ Передвигает все нули в конец заданного списка """
    new_arr = []
    for item in array:
        if item != 0 or item is False:
            new_arr.append(item)
    return new_arr + [0] * (len(array) - len(new_arr))
