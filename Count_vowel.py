def get_count (input_str):
    """Отображает количество согласных (кроме y). Английская
    раскладка."""
    num_vowels = 0
    vowels = ['a', 'e', 'i', 'o', 'u']
    for i in input_str:
        if i in vowels:
            num_vowels += 1
    return num_vowels
