import string


class Alphabet:

    def __init__(self, lang, letters):
        self.lang = lang
        self.letters = list(letters)

    def print(self):
        print(self.letters)

    def letters_num(self):
        return len(self.letters)


class EngAlphabet(Alphabet):

    __letters_num = 26

    def __init__(self):
        super().__init__('En', string.ascii_uppercase)

    def is_en_letter(self, letter):
        if letter.upper() in self.letters:
            return True
        return False

    def letters_num(self):
        return EngAlphabet.__letters_num

    @staticmethod
    def example():
        print('Hello, friend!')


if __name__ == '__main__':
    my_alphabet = EngAlphabet()
    my_alphabet.print()
    print(my_alphabet.letters_num())
    print(my_alphabet.is_en_letter('F'))
    print(my_alphabet.is_en_letter('Д'))
    EngAlphabet.example()