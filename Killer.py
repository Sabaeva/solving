def killer (suspect_info, dead):
    """Дается словарь с именами подозреваемых и именами тех, кого они
     видели в день убийства, и лист с именами убитых, если имена в
     списках совпали, вернуть имя убийцы"""
    return [key for key, value in suspect_info.items() if len(set(
        dead) & set(value)) == len(dead)][0]
