def duplicate_count(text):
    """Возвращает количество символов, встречающихся в тексте более
    одного раза"""
    i=0
    t=list(text.lower())
    for item in sorted(t):
        if t.count(item)>1:
            i+=1
            for k in range(0,t.count(item)):
                t.remove(item)
        else:
            continue
    return i