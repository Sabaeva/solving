class Human:
    default_name = None
    default_age = 0

    def __init__(self, name=default_name, age=default_age):
        self.name = name
        self.age = age
        self._money = 0
        self._house = None

    def info(self):
        print(f'Name: {self.name}\nAge: {self.age}\nMoney: {self._money} '
              f'\nHouse: {self._house}\n')

    @staticmethod
    def default_info():
        print(f'Default name: {Human.default_name}'
              f'\nDefault age: {Human.default_age}')

    def make_deal(self, new_house, cost):
        """ Реализует сделку по покупке дома """
        self._money -=cost
        self._house = new_house
        print(f'Стоимость дома со скидкой: {cost}\n'
              f'После покупки на счету: {self._money}')

    def earn_money(self, new_money):
        """ ДОбавляет заработанные деньги на счет """
        self._money += new_money
        print(f'Заработано: {new_money}\nНа счету: {self._money}')

    def buy_house(self, new_house, discount):
        """ Проверяет, достаточно ли денег для покупки, в случае
        положительного ответа вызывает метод для покупки дома """
        cost = new_house.final_cost(discount)
        if self._money >= cost:
            self.make_deal(new_house, cost)
        else:
            print('Недостаточно денег на счету!')

class House:

    def __init__(self, area, cost):
        self._area = area
        self._cost = cost

    def final_cost(self, discount):
        """ Высчитывает окончательную стоимость с учетом скидки"""
        final = self._cost * (100-discount)/100
        return final

class SmallHouse(House):

    def __init__(self, cost):
        """ Переопределяет параметр площади """
        super().__init__(40, cost)


if __name__ == '__main__':

    Human.default_info()
    my_human = Human('Anna', 29)
    my_human.info()
    small_house = SmallHouse(8000)
    my_human.buy_house(small_house, 5)
    my_human.earn_money(20000)
    my_human.buy_house(small_house, 5)
    my_human.earn_money(40000)
    my_human.buy_house(small_house, 5)
    my_human.info()